//
//  ViewController.swift
//  TableViewReview
//
//  Created by MacStudent on 2019-06-25.
//  Copyright © 2019 rabbit. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }

    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    @IBOutlet weak var messageLbl: UILabel!
    
    
    @IBAction func sendMessage(_ sender: UIButton) {
        print("button pressed")
        
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let abc = [
                "Message":"Hello Watch"
            ]
            // send the message to the watch
            WCSession.default.sendMessage(abc, replyHandler: nil)
        }
        else {
            print("PHONE: Cannot find the watch")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        // output a debug message to the terminal
        print("PHONE: Got a message!")
        print(message)
        
        let m = "\(message["Message"] as! String)"
        // update the message with a label
        self.messageLbl.text = m
        
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let abc = [
                "Message":"Hi Watch, i received your message."
            ]
            // send the message to the watch
            WCSession.default.sendMessage(abc, replyHandler: nil)
        }
        else {
            print("PHONE: Cannot find the watch")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

