# README #

MADT 5314 - Activity 2 (6%)
Watch <> Phone communication

Joao Rebelo

C0706998

### Theoretical Questions ###
**On the IOS side, what function is used to receive messages from the watch?**

the function is:

*func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {}*

**When receiving messages from the watch, you need to use a closure function. What is the reason why a closure is used?**

I didn't used any closure function but i could have used the replyHandler that after receive the message sends a message back to notify/reply the sender.
