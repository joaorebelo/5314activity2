//
//  TextInputDemoInterfaceController.swift
//  TableViewReview
//
//  Created by MacStudent on 2019-06-25.
//  Copyright © 2019 rabbit. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class TextInputDemoInterfaceController: WKInterfaceController, WCSessionDelegate {

    // MARK: OUTLETS
    
    @IBOutlet weak var outputLabel: WKInterfaceLabel!
    
    // MARK: Required function for WCSessionDelegate
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
  
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        // output a debug message to the terminal
        print("WATCH: Got a message!")
        print(message)
        
        // update the message with a label
        self.outputLabel.setText("\(message["Message"] as! String)")
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    // MARK: Actions
    // --------------------
    
    @IBAction func inputButtonPressed() {
        
        print("button pressed")
        
        // check if the phone is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let abc = [
                "Message":"Hello Phone"
            ]
            // send the message to the phone
            WCSession.default.sendMessage(abc, replyHandler: nil)
        }
        else {
            print("WATCH: Cannot find the phone")
        }
    /*
        let aaaaaaaa = ["In class", "Doing Assignment", "At movies", "Sleeping"]
        
        self.presentTextInputController(
        withSuggestions: aaaaaaaa,
        allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                print(results!)
                
                let response = results?[0] as! String
                
                // WRONG: outputLabel.text = "\(results)"
                self.outputLabel.setText(response)
                
            }
        }*/
    }
    
    
    
    
    

}
