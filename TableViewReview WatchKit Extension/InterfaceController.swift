//
//  InterfaceController.swift
//  TableViewReview WatchKit Extension
//
//  Created by MacStudent on 2019-06-25.
//  Copyright © 2019 rabbit. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    // MARK: Outlets
    // ------------------------
    @IBOutlet weak var gamesTable: WKInterfaceTable!
    
    
    // MARK: Data Source
    // ------------------------
    var gamesList:[String] = ["Wax the Leg", "Catch the Lemming",
    "Save the Banana","Fly Chakli Fly", "abcd", "dddd"]
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        // 1. tell watch how many rows you want in table
        self.gamesTable.setNumberOfRows(
            self.gamesList.count, withRowType:"myRow"
        )
        
        // 1. tell watch what data goes in each row
       
        for (index, x) in self.gamesList.enumerated() {
            print("\(index), \(x)")
            let row = self.gamesTable.rowController(at: index) as! GamesRowController
            row.gameNameLabel.setText(x)
        }
        
        
        
        
        
        
    
        
        /*
        for (int i = 0; i < self.gamesList.count; i++) {
            let row = self.gamesTable.rowController(at: i) as!
         
         
         
         
         s
         dfshjsdfhjkGamesRowController
            row.gameNameLabel.setText(self.gamesList[i])
        }*/
        
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
