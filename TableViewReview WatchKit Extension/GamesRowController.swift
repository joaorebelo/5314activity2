//
//  GamesRowController.swift
//  TableViewReview
//
//  Created by MacStudent on 2019-06-25.
//  Copyright © 2019 rabbit. All rights reserved.
//

import WatchKit

class GamesRowController: NSObject {

    // MARK: Outlets
    // --------------------------
    @IBOutlet weak var gameImage: WKInterfaceImage!
    @IBOutlet weak var gameNameLabel: WKInterfaceLabel!
    
}
